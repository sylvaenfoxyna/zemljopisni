#include <iostream>
#include <csignal>
#include <ctime>
#include <unistd.h>
using namespace std;


int priori;

void signalfun(int signal_number)
{
    sighold(signal_number);
    int temp;
    time_t t;
    
    time(&t);
    cout<<"Prekid pokusan "<<ctime(&t)<<endl;
    
    cout<<endl;
    switch(signal_number)
    {
        case SIGINT:
            temp=1;
            break;

        case SIGQUIT:
            temp=2;
            break;

        case SIGTSTP:
            temp=3;
            break;
        
        default:
            return;
    }
    
    if(priori<temp)
    {
        sigrelse(signal_number);
        priori=temp;
        cout<<"\n Prioritet "<<priori<<" me ubio \n \n";

        for (int i=1; i<=5; i++)
        {
            priori=temp;
            cout<<i<<"s \n";
            sleep(1);
        }
        cout<<endl;
    }

    sigrelse(signal_number);
}


int main ()
{
   priori=0;

   sigset(SIGINT, signalfun);
   sigset(SIGQUIT, signalfun);
   sigset(SIGTSTP, signalfun);

   for(int i=1; i<=30; i++)
   {
      priori=0;
      sleep(1);
      cout<<"("<<i<<"/30) \n";
   }

   return 0;
}
